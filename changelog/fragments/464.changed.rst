API changes:

- Rename :func:`.iks_agg` to :func:`.compute_iks_agg`
- Rename :func:`.iks_agg_jac_v` to :func:`.compute_total_iks_agg_jac`
- Rename :func:`.ks_agg` to :func:`.compute_ks_agg`
- Rename :func:`.ks_agg_jac_v` to :func:`.compute_total_ks_agg_jac`
- Rename :func:`.max_agg` to :func:`.compute_max_agg`
- Rename :func:`.max_agg_jac_v` to :func:`.compute_max_agg_jac`
- Rename :func:`.sum_square_agg` to :func:`.compute_sum_square_agg`
- Rename :func:`.sum_square_agg_jac_v` to :func:`.compute_total_sum_square_agg_jac`
- Rename the first positional argument ``constr_data_names`` of :class:`.ConstrAggegationDisc` to ``constraint_names``.
- Rename the second positional argument ``method_name`` of :class:`.ConstrAggegationDisc` to ``aggregation_function``.
- Rename the first position argument ``constr_id`` of :meth:`.OptimizationProblem.aggregate_constraint` to ``constraint_index``.
- Rename the aggregation methods ``"pos_sum"``, ``"sum"`` and ``"max"`` to ``"POS_SUM"``, ``"SUM"`` and ``"MAX"``.
- The name of the method to evaluate the quality measure is passed to :class:`.MLAlgoAssessor` with the argument ``measure_evaluation_method``.
- The name of the method to evaluate the quality measure is passed to :class:`.MLAlgoSelection` with the argument ``measure_evaluation_method``.
- The name of the method to evaluate the quality measure is passed to :class:`.MLAlgoCalibration` with the argument ``measure_evaluation_method``.

The names of the methods to evaluate a quality measure can be accessed with :attr:`.MLAlgoQualityMeasure.EvaluationMethod`.
