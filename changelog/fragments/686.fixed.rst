Modify the computation of total derivatives in the presence of state variables to avoid unnecessary calculations.
